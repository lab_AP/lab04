#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void allocate (char** mat, char* line, int i);

int main(int argc, char *argv[])
{
  if (argc !=3){
    printf("Usage: %s file1 file2\n", argv[0]);
    exit(1);
  }

  FILE* input;
  FILE* output;
  char line[100];
  char **mat;
  int number;
  int i = 0;

  input = fopen(argv[1], "r");
  output = fopen(argv[2], "r+");
  if (input == NULL || output == NULL){
    printf("Error while opening files.\n");
    exit(1);
  }
  fscanf(input, "%d", &number);
  fprintf(output, "%d\n", number);
  mat = malloc(number * sizeof(char *));
  if (mat == NULL){
    printf("Error allocating mat");
    exit(1);
  }

  while(fscanf(input, "%s", line) != EOF && i<number)
  {
    allocate(mat, line, i);
    i++;
  }

  for(i=0; i<number;i++){
    fprintf(output, "%s", mat[i]);
  }

  return 1;
}

void allocate (char** mat, char* line, int i)
{
  int len;
  len = strlen(line);
  mat[i] = malloc(len*sizeof(char)+1);
  if (mat[i] == NULL){
    printf("Error allocating line %d in mat", i);
    exit(1);
  }
}

void copy(FILE* output, char* line)
{
  fprintf(output, "%s\n", line);
}
